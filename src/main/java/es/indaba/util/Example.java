package es.indaba.util;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import es.indaba.util.concurrent.DefaultPromise;
import es.indaba.util.concurrent.Promise;

public class Example {
    
    private static final int THREAD_POOL_SIZE = 2;
    
    private static Executor pool = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
    
    
    public static void main(String[] args) {
        
        Promise<Void> promise = new DefaultPromise<>();
        
        LongTask longTask = new LongTask(promise);
        ShortTask shortTask = new ShortTask(promise);
        
        pool.execute(longTask);
        pool.execute(shortTask);
    }
    
    
    private static class LongTask implements Runnable {

        private Promise promise;
        
        public LongTask(Promise promise) {
            this.promise = promise;
        }
        
        @Override
        public void run() {
            try {
                Thread.sleep(10000);
                this.promise.setSuccess(null);
            } catch (InterruptedException e) {
                this.promise.setFailure(e);
                e.printStackTrace();
            }
        }
        
    }
    

    private static class ShortTask implements Runnable {

        private Promise promise;
        
        public ShortTask(Promise promise) {
            this.promise = promise;
        }
        
        @Override
        public void run() {
            try {
                Thread.sleep(5000);
                this.promise.setSuccess(null);
            } catch (InterruptedException e) {
                this.promise.setFailure(e);
                e.printStackTrace();
            }
        }
        
    }
    
    

}
