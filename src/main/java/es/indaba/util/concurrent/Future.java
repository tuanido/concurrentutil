package es.indaba.util.concurrent;

import java.util.concurrent.TimeUnit;

/**
 * Representa el resultado de una operación asíncrona.
 * 
 * @param <V>
 *            El tipo del resultado devuelto por la operación.
 */
public interface Future<V> extends java.util.concurrent.Future<V> {

    /**
     * Espera a que este {@link Future} sea marcado como completado.
     * 
     * @return Devuelve el {@link Future} por el que hemos estado esperando.
     * 
     * @throws InterruptedException
     *             Si el hilo de ejecución actual es interrumpido.
     */
    Future<V> await() throws InterruptedException;

    /**
     * Espera a que este {@link Future} sea marcado como completado dentro del
     * tiempo especificado.
     * 
     * @param timeout
     *            Cantidad de tiempo a esperar.
     * @param unit
     *            Unidad de la cantidad de tiempo a esperar.
     * 
     * @return {@code true} si y sólo si este {@link Future} es marcado como
     *         completado dentro del tiempo especificado.
     * 
     * @throws InterruptedException
     *             Si el hilo de ejecución actual es interrumpido.
     */
    boolean await(long timeout, TimeUnit unit) throws InterruptedException;

    /**
     * Devuelva la causa del fallo.
     * 
     * @return La causa del fallo. {@code null} si este {@link Future} ha sido
     *         marcado como exitoso o si aún no se ha completado.
     */
    Throwable cause();

    /**
     * <p>
     * Devuelve el resultado sin bloquear. Si este {@link Future} no ha sido aún
     * marcado como completado devolverá {@code null}.
     * </p>
     * 
     * <p>
     * Como es posible que un valor {@code null} sea utilizado para marcar este
     * {@link Future} como completado con exito también deberemos comprobar que
     * este {@link Future} está realmente completado invocando el método
     * {@link #isDone()} y no fiarnos unicamente del hecho de que este método
     * haya devuelto un valor {@code null}.
     * </p>
     * 
     * @return El resultado de este {@link Future}. {@code null} si este
     *         {@link Future} no se ha completado aún.
     */
    V getNow();
}