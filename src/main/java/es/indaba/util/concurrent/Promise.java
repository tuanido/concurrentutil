package es.indaba.util.concurrent;

public interface Promise<V> extends Future<V> {

    /**
     * Marca este {@link Promise} como exitoso.
     * 
     * @param result
     *            Valor a devolver.
     * 
     * @return El {@link Promise} exitoso.
     * 
     * @throw IllegalStateException Si ya ha sido marcado como exitoso o
     *        fallido.
     */
    Promise<V> setSuccess(V result);

    /**
     * Marca este {@link Promise} como fallido.
     *
     * @param cause
     *            Causa del fallo.
     * 
     * @return El {@link Promise} fallido.
     *
     * @throw IllegalStateException Si ya ha sido marcado como exitoso o
     *        fallido.
     */
    Promise<V> setFailure(Throwable cause);
}