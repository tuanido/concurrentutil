package es.indaba.util.concurrent;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import es.indaba.util.StringUtils;

/**
 * Un {@link Error} especial que es lanzado para notificar un estado.
 * {@link Signal} tiene una pila de seguimiento vacía y no tiene causa raíz para
 * evitar la sobrecarga que supone la inicialización de estos.
 */
public class Signal extends Error {

    private static final long serialVersionUID = 1L;

    /**
     * <p>
     * Este mapa guarda los nombres que ya han sido utilizados con anterioridad
     * para crear un {@link Signal}.
     * </p>
     * 
     * <p>
     * <strong>IMPORTANTE:</strong> al ser un objeto estático pertenece a la
     * clase no a las instancias. Es decir, todos las instancias de
     * {@link Signal} compartirán el mismo mapa.
     * </p> 
     */
    private static final ConcurrentMap<String, Boolean> map = new ConcurrentHashMap<String, Boolean>();

    /**
     * El nombre del {@link Signal}
     * 
     * <p>
     * <strong>IMPORTANTE:</strong> no puede haber dos {@link Signal} con el
     * mismo nombre.
     * </p> 
     */
    private final String name;

    /**
     * Constructor de la clase.
     * 
     * @param name
     *            El nombre del nuevo {@link Signal}.
     * 
     * @throws NullPointerException
     *             Si el nombre es nulo o vacío.
     * @throws IllegalArgumentException
     *             Si el nombre ha sido utilizado con anterioridad para crear
     *             otro {@link Signal}.
     */
    private Signal(String name) {

	super(name);

    if (StringUtils.isBlank(name)) {
	    throw new NullPointerException("'name' must not be null or empty.");
	}

	if (map.putIfAbsent(name, Boolean.TRUE) != null) {
	    throw new IllegalArgumentException(String.format("'%s' is already in use", name));
	}

	this.name = name;
    }

    /**
     * Crea un nuevo {@link Signal} usando el nombre especificado.
     * 
     * @param name
     *            El nombre para nuevo {@link Signal}.
     * 
     * @return Un nuevo {@link Signal}.
     */
    public static Signal valueOf(String name) {
	return new Signal(name);
    }

    /**
     * Comprueba que el {@link Signal} dado es el mismo que esta instancia.
     * 
     * @param signal
     *            El {@link Signal} a comparar con la instancia actual.
     * 
     * @throws IllegalStateException
     *             Si no es la es igual.
     */
    public void expect(Signal signal) {
	if (this != signal) {
	    throw new IllegalStateException(String.format("unexpected signal: '%s'", signal.toString()));
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Throwable#initCause(Throwable)
     */
    @Override
    public Throwable initCause(Throwable cause) {
	return this;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Throwable#fillInStackTrace()
     */
    @Override
    public Throwable fillInStackTrace() {
	return this;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Throwable#toString()
     */
    @Override
    public String toString() {
	return name;
    }
}