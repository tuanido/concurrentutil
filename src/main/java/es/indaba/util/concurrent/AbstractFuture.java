package es.indaba.util.concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Implementación abstracta de {@link Future}.
 * 
 * @param <V>
 *            El tipo del resultado devuelto por la operación.
 */
public abstract class AbstractFuture<V> implements Future<V> {

    // -------------------------------------------------------------------
    // Future Implementation
    // -------------------------------------------------------------------

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Future#get()
     */
    @Override
    public V get() throws InterruptedException, ExecutionException {

	await();

	Throwable cause = cause();
	if (cause == null) {
	    return getNow();
	}

	throw new ExecutionException(cause);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Future#get(Long, TimeUnit)
     */
    @Override
    public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {

	if (await(timeout, unit)) {

	    Throwable cause = cause();
	    if (cause == null) {
		return getNow();
	    }

	    throw new ExecutionException(cause);
	}

	throw new TimeoutException("Timed out waiting for response.");
    }
}