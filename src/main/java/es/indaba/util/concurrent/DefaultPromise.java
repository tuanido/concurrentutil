package es.indaba.util.concurrent;

import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;

/**
 * Implementación de {@link Promise}.
 * 
 * @param <V>
 *            El tipo del valor devuelto.
 */
public class DefaultPromise<V> extends AbstractFuture<V> implements Promise<V> {

    /**
     * Señal que indica que este {@link Future} ha sido completado con éxito
     * pero que el resultado es un valor nulo.
     */
    private static final Signal SUCCESS = Signal.valueOf(DefaultPromise.class.getName() + ".SUCCESS");

    /**
     * Excepción que representa la cancelación de este {@link Future}.
     */
    private static final CauseHolder CANCELLATION_CAUSE_HOLDER = new CauseHolder(new CancellationException());

    /**
     * El resultado de este {@link Future}.
     */
    private volatile Object result;

    /**
     * Número de operaciones esperando a que se complete este {@link Future}.
     */
    private short waiters;

    /**
     * Inicializador estático que vacía la pila de seguimiento de la excepción
     * que representa la cancelación de este {@link Future}.
     */
    static {
	CANCELLATION_CAUSE_HOLDER.cause.setStackTrace(new StackTraceElement[0]);
    }

    // -------------------------------------------------------------------
    // Future Implementation
    // -------------------------------------------------------------------

    /*
     * (non-Javadoc)
     * 
     * @see
     * es.indaba.util.concurrent.Future#await()
     */
    @Override
    public Future<V> await() throws InterruptedException {

	if (isDone()) {
	    return this;
	}

	if (Thread.interrupted()) {
	    throw new InterruptedException(toString());
	}

	synchronized (this) {
	    while (!isDone()) {
		incWaiters();
		try {
		    wait();
		} 
		finally {
		    decWaiters();
		}
	    }
	}
	return this;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * es.indaba.util.concurrent.Future#await(Long,
     * TimeUnit)
     */
    @Override
    public boolean await(long timeout, TimeUnit unit) throws InterruptedException {
	return await0(unit.toNanos(timeout), true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * es.indaba.util.concurrent.Future#cause()
     */
    @Override
    public Throwable cause() {

	Object result = this.result;

	if (result instanceof CauseHolder) {
	    return ((CauseHolder) result).cause;
	}

	return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * es.indaba.util.concurrent.Future#getNow()
     */
    @Override
    @SuppressWarnings("unchecked")
    public V getNow() {

	Object result = this.result;

	if (result instanceof CauseHolder || result == SUCCESS) {
	    return null;
	}

	return (V) result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Future#cancel(Boolean)
     */
    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {

	Object result = this.result;

	if (isDone0(result)) {
	    return false;
	}

	synchronized (this) {
	    result = this.result;

	    if (isDone0(result)) {
		return false;
	    }

	    this.result = CANCELLATION_CAUSE_HOLDER;

	    if (hasWaiters()) {
		notifyAll();
	    }
	}

	return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Future#isCancelled()
     */
    @Override
    public boolean isCancelled() {
	return isCancelled0(result);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.concurrent.Future#isDone()
     */
    @Override
    public boolean isDone() {
	return isDone0(result);
    }

    // -------------------------------------------------------------------
    // Promise Implementation
    // -------------------------------------------------------------------

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cafpowerandautomation.cmts.remoting.mce.concurrent.Promise#setSuccess
     * (*
     */
    @Override
    public Promise<V> setSuccess(V result) {
	if (setSuccess0(result)) {
	    return this;
	}
	throw new IllegalStateException("complete already: " + this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * es.indaba.util.concurrent.Promise#setFailure
     * (*
     */
    @Override
    public Promise<V> setFailure(Throwable cause) {
	if (setFailure0(cause)) {
	    return this;
	}
	throw new IllegalStateException("complete already: " + this, cause);
    }

    // -------------------------------------------------------------------
    // Private Methods
    // -------------------------------------------------------------------

    /**
     * Objeto wrapper que encapsula la causa del fallo de este {@link Future}.
     */
    private static final class CauseHolder {

	/**
	 * La causa del fallo de este {@link Future}
	 */
	final Throwable cause;

	/**
	 * Constructor de la clase.
	 * 
	 * @param cause
	 *            La causa del fallo de este {@link Future}
	 */
	private CauseHolder(Throwable cause) {
	    this.cause = cause;
	}
    }

    /**
     * Espera a que este {@link Future} sea marcado como completado dentro del
     * tiempo especificado.
     * 
     * @param timeoutNanos
     *            Cantidad de tiempo a esperar en nanosegundos.
     * @param interruptable
     *            Especifica si la espera puede ser o no interrumpida.
     * 
     * @return {@code true} si y sólo si este {@link Future} es marcado como
     *         completado dentro del tiempo especificado.
     * 
     * @throws InterruptedException
     *             Si el hilo de ejecución actual es interrumpido.
     */
    private boolean await0(long timeoutNanos, boolean interruptable) throws InterruptedException {

	if (isDone()) {
	    return true;
	}

	if (timeoutNanos <= 0) {
	    return isDone();
	}

	if (interruptable && Thread.interrupted()) {
	    throw new InterruptedException(toString());
	}

	long startTime = System.nanoTime();
	long waitTime = timeoutNanos;
	boolean interrupted = false;

	try {
	    synchronized (this) {

		if (isDone()) {
		    return true;
		}

		if (waitTime <= 0) {
		    return isDone();
		}

		incWaiters();
		try {
		    for (;;) {
			try {
			    wait(waitTime / 1000000, (int) (waitTime % 1000000));
			} 
			catch (InterruptedException e) {
			    if (interruptable) {
				throw e;
			    } 
			    else {
				interrupted = true;
			    }
			}

			if (isDone()) {
			    return true;
			} 
			else {
			    waitTime = timeoutNanos - (System.nanoTime() - startTime);
			    if (waitTime <= 0) {
				return isDone();
			    }
			}
		    }
		} 
		finally {
		    decWaiters();
		}
	    }
	} 
	finally {
	    if (interrupted) {
		Thread.currentThread().interrupt();
	    }
	}
    }

    /**
     * <p>
     * Devuelve {@code true} si este {@link Future} está completado.
     * </p>
     * 
     * <p>
     * Este {@link Future} puede estar completado debido a la finalización
     * normal de la tarea, a una excepción o debido a una cancelación. En todos
     * estos casos este metodo devolverá {@code true}.
     * </p>
     * 
     * @param result
     *            El resultado de este {@link Future}.
     * 
     * @return {@code true} si está completado.
     */
    private static boolean isDone0(Object result) {
	return result != null;
    }

    /**
     * Devuelve {@code true} si este {@link Future} fue cancelado antes de que
     * finalizará.
     * 
     * @param result
     *            El resultado de este {@link Future}.
     * 
     * @return {@code true} si fue cancelado antes de que finalizará.
     */
    private static boolean isCancelled0(Object result) {
	return result instanceof CauseHolder
		&& ((CauseHolder) result).cause instanceof CancellationException;
    }

    /**
     * Marca este {@link Future} como exitoso.
     * 
     * @param result
     *            El nuevo valor para el resultado de este {@link Future}.
     * 
     * @return {@code true} si se ha podido establecer el nuevo valor con exito.
     *         {@code false} si este {@link Future} ya ha sido marcado como
     *         exitoso o fallido con anterioridad.
     */
    private boolean setSuccess0(V result) {

	if (isDone()) {
	    return false;
	}

	synchronized (this) {

	    if (isDone()) {
		return false;
	    }

	    if (result == null) {
		this.result = SUCCESS;
	    } 
	    else {
		this.result = result;
	    }

	    if (hasWaiters()) {
		notifyAll();
	    }
	}
	return true;
    }

    /**
     * Marca este {@link Future} como fallido.
     * 
     * @param cause
     *            La causa del fallo.
     * 
     * @return {@code true} si se ha podido establecer la causa del fallo con
     *         exito. {@code false} si este {@link Future} ya ha sido marcado
     *         como exitoso o fallido con anterioridad.
     */
    private boolean setFailure0(Throwable cause) {

	if (isDone()) {
	    return false;
	}

	synchronized (this) {

	    if (isDone()) {
		return false;
	    }

	    result = new CauseHolder(cause);

	    if (hasWaiters()) {
		notifyAll();
	    }
	}
	return true;
    }

    /**
     * Devuelve {@code true} si hay alguien esperando por este {@link Future}.
     * 
     * @return {@code true} si hay alguien esperando por este {@link Future}.
     */
    private boolean hasWaiters() {
	return waiters > 0;
    }

    /**
     * Incrementa el número de procesos que esperan por este {@link Future}.
     */
    private void incWaiters() {
	if (waiters == Short.MAX_VALUE) {
	    throw new IllegalStateException("too many waiters: " + this);
	}
	waiters++;
    }

    /**
     * Decrementa el número de procesos que esperan por este {@link Future}.
     */
    private void decWaiters() {
	waiters--;
    }
}