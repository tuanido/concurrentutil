package es.indaba.util;

import java.util.concurrent.Future;

import org.junit.Assert;
import org.junit.Test;

import es.indaba.util.concurrent.DefaultPromise;
import es.indaba.util.concurrent.Promise;

public class ExampleTest {

    @Test
    public void rejectFutureOnPresent () throws InterruptedException {
        
        FutureTask futureTask = createTask(new DefaultPromise<Void>(), 3000l);
        
        Thread thread = new Thread(futureTask);
        thread.start();
        
        futureTask.getPromise().setFailure(new InterruptedException("Future rejected on present."));
        
        thread.join();
        
        if (futureTask.getPromise().isDone()) {
            Assert.assertTrue(futureTask.getPromise().cause() != null);
            System.out.println(futureTask.getPromise().cause().getMessage());
        } 
        else {
            Assert.fail();
        }
    }

    @Test
    public void awaitForTheFuture () throws InterruptedException {
        
        FutureTask futureTask = createTask(new DefaultPromise<Void>(), 10000l);
        
        Thread thread = new Thread(futureTask);
        thread.start();
        
        Future<Void> future = futureTask.getPromise().await();
        Assert.assertTrue(future.isDone());
    }
    
    

    private FutureTask createTask (Promise<Void> promise, Long duration) {
        return new FutureTask(promise, duration);
    }

    private final static class FutureTask implements Runnable {

        private Promise<Void> promise;
        private Long duration;

        public FutureTask(Promise<Void> promise, Long duration) {
            
            this.promise = promise;
            this.duration = duration;
        }

        @Override
        public void run() {
            
            System.out.println("RUNNING");
            try {
                
                Thread.sleep(this.duration);
                
                if (!this.promise.isDone()) {
                    
                    this.promise.setSuccess(null);
                }
            } 
            catch (InterruptedException e) {
                
                this.promise.setFailure(e);
            } 
            finally {
                
                System.out.println("DONE");
            }
        }
        
        public Promise<Void> getPromise () {
            
            return this.promise;
        }

    }

}
